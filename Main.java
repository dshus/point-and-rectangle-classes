import tinshoes.geom.Rectangle;
import java.util.Scanner;
import tinshoes.input.SafeScanner;
import tinshoes.geom.Point;
public class Main {
	public static void main(String[] args) {
		Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
		String errText = " is not valid input.";
		String prompt = "Enter a value for the coordinates.";
		Point[] pts = new Point[4];
		Rectangle rect = new Rectangle(new Point(0, 0), new Point(1, 1)); //Initialization
		try {
			System.out.println("Enter the 2 or 4 points for the rectangle:");
			for (int i = 0; i < 4; i++) {
				pts[i] = new Point(sc.nextDouble("Enter the x-value for point " + (i + 1) + ".", errText, true), sc.nextDouble("Enter the y-value for point " + (i + 1) + ".", errText, true));
				if (i == 1) {
					if (!sc.yn("Would you like to continue?", "That is not valid input.")) {
						rect = new Rectangle(pts[0], pts[1]);
						break;
					}
				}
				if (i == 3) {
					rect = new Rectangle(pts[0], pts[1], pts[2], pts[3]);
				}
			}
			System.out.println("Your rectangle is " + (rect.isParallelToAxes() ? "" : "not ") + "parallel to the global axes.");
			System.out.println("Now enter a point to test.");
			System.out.println("The rectangle does " + (rect.contains(new Point(sc.nextDouble("Enter the x-value.", errText, true), sc.nextDouble("Enter the y-value.", errText, true))) ? "" : "not ") + "contain that point.");
			System.out.println("The rectangle can be represented like this:\n" + rect.toString());
			if (!sc.yn("Would you like to continue to midpoints?", "That is not valid input."))
				return;
			System.out.println("Enter the points.");
			System.out.println(Point.midpoint(new Point(sc.nextDouble("Enter an x-value.", errText, true), sc.nextDouble("Enter a y-value.", errText, true)), new Point(sc.nextDouble("Enter an x-value.", errText, true), sc.nextDouble("Enter a y-value.", errText, true))));
		} catch (Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
	}
}